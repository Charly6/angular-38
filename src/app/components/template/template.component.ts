import { Component, OnInit } from '@angular/core';
import { IPersona } from 'src/app/interfaces/persona.interface';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  persona : IPersona = {
    nombre: "",
    direccion: "",
    terminos: ''
  }

  constructor() { }

  guardar():void {
    console.log('guardando');
    console.log(this.persona);
    
  }

  ngOnInit(): void {
  }

}
